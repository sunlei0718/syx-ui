import syxCeshi from './syxCeshi'
import syxTree from './syxTree'
import syxMenuSun from './syxMenuSun'
import syxButtonGroup from './syxButtonGroup'
import syxAround from './syxAround'
import syxFlexs from './syxFlexs'
import syxPagination from './syxPagination'
import syxPopup from './syxPopup'
import syxList from './syxList'
import syxCalendar from './syxCalendar/index.js';
import syxWeekCalendar from './syxWeekCalendar/index.js';

// 组件列表
const components = [
    syxCeshi,
    syxTree,
    syxMenuSun,
    syxButtonGroup,
    syxList,
    syxPopup,
    syxPagination,
    syxFlexs,
    syxAround,
    syxCalendar,
    syxWeekCalendar
]

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，那么所有的组件都会被注册
const install = function (Vue) {
    // 判断是否安装
    if (install.installed) return
    // 遍历注册全局组件
    components.map(component => Vue.component(component.name, component))
}

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}

export default {
    // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
    install,
    // 以下是具体的组件列表
    syxCeshi,
    syxTree,
    syxMenuSun,
    syxButtonGroup,
    syxList,
    syxPopup,
    syxPagination,
    syxFlexs,
    syxAround,
    syxCalendar,
    syxWeekCalendar
}
