import Vue from "vue";
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Demo from "./demo.vue";

Vue.config.productionTip = false;
// 导入组件库
import syxUI from './index'
// 注册组件库
Vue.use(syxUI)
Vue.use(ElementUI)
new Vue({
  render: h => h(Demo)
}).$mount("#app");
