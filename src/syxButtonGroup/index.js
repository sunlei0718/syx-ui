// 导入组件，组件必须声明 name
import sub from './indt'

// 为组件提供 install 安装方法，供按需引入
sub.install = function (Vue) {
    Vue.component(sub.name, sub)
}

// 导出组件
export default sub
