/**
 * 处理一周的时间
 * @param {*} param 日期可以是时间对象,可以是YYYY-MM-DD的
*/
export default function (param = new Date()) {
    let newDate = new Date(param);
    let year1 = newDate.getFullYear();
    let year2 = newDate.getFullYear();
    let month1 = newDate.getMonth();
    let month2 = newDate.getMonth();
    let date1 = newDate.getDate();
    let date2 = newDate.getDate();
    let days = [7, 1, 2, 3, 4, 5, 6];
    let day_k = newDate.getDay();
    let day = days[day_k];
    const isLeapYear = (year) => {
        if (((year % 4) == 0) && ((year % 100) != 0) || ((year % 400) == 0)) {
            return 1;
        } else {
            return 0;
        }
    }
    const addZero = (n) =>{
        if(n<10){
            return '0'+n
        }else{
            return n;
        }
    }
    const dates1 = [31, 28 + isLeapYear(year1), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];       //所有月份的天数,这里条用的一个函数用于计算闰年
    const dates2 = [31, 28 + isLeapYear(year1), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];       //所有月份的天数,这里条用的一个函数用于计算闰年
    let start = [];
    for (let i = 0; i < day; i++) {
        let _day = day - i;
        if (date1 < 1) {
            month1 = month1 - 1;
            if (month1 < 0) {
                month1 = 11;
                year1 = year1 - 1;
                dates1[1] = (28 + isLeapYear(year1));
            }
            date1 = dates1[month1];
        }
        start.push({
            date: date1,
            day: _day,
            month: month1,
            year: year1
        })
        date1 -= 1;
    }
    let end = [];
    for (let i = 1; i <= 7 - day; i++) {
        let _day = day + i;
        date2 += 1;
        if (date2 > dates2[month2]) {
            month2 = month2 + 1;
            if (month2 > 11) {
                month2 = 0;
                year2 = year2 + 1;
                dates2[1] = (28 + isLeapYear(year2));
            }
            date2 = 1;
        }
        end.push({
            date: date2,
            day: _day,
            month: month2,
            year: year2
        })
    }
    let all = start.concat(end);
    all.forEach((v)=>{
        v.dateStr = `${v.year}-${addZero((v.month+1))}-${addZero(v.date)}`;
    })
    all.sort((a, b) => a.day - b.day);
    return all;
}