---
home: true

actionText: 快速上手 →
actionLink: /guide/
features:
- title: 简易好学
  details: 基于 element-ui 的开发 , ^2.13.2
- title: Vue 驱动
  details: 享受 Vue + element-ui 的开发体验，实践最流行的技术。
- title: syx-ui
  details: syx-ui
footer: Sun 2294073848@qq.com
---
