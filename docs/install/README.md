---
title: 安装
---

# 安装

推荐安装  syx-ui@0.0.24

```bash
npm install syx-ui@0.0.24
```

或

```bash
yarn add syx-ui@0.0.24
```
