---
title: 快速上手
---

## 安装

请参考 [安装](../install/) 章节


## 单个引入

```javascript
import syxUI from 'syx-ui/lib/syx.umd.min'

import Vue from 'vue'

new Vue({
  el: '#app',
  components: {syxCeshi: syxUI.syxCeshi}
})

```
## 全局引入
   
```javascript
    import syxUI from 'syx-ui/lib/syx.umd.min'
    import 'syx-ui/lib/syx.css'
    Vue.use(syxUI)
   
```
