const path = require('path')
module.exports = {
    base: '/syxUI/',
    title: '❤ Syx UI',
    description: '一套为开发者学习准备的基于 Vue 2.0 和 ElementUI组件库',
    head: [
        ['link', {rel: 'icon', href: '/img/s.png', type: 'image/png'}]
    ],
    themeConfig: {
        repo: true,// 默认是 false, 设置为 true 来启用
        lastUpdated: 'Sun',
        // editLinks: true,
        nav: [
            {text: '主页', link: '/'},
            {text: '指南', link: '/guide/'},
            {text: '交流', link: 'https://gitee.com/sunlei0718/syx-ui'},
        ],
        sidebarDepth: 2,
        sidebar: [
            {
                title: '入门',
                collapsable: false,
                children: [
                    '/install/',
                    '/get-started/',
                ]
            },
            {
                title: '组件',
                collapsable: false,
                children: [
                    '/components/syx-ceshi',
                    '/components/syx-tree',
                    '/components/syx-menusun',
                    '/components/syx-buttongroup',
                    '/components/syx-around',
                    '/components/syx-flexs',
                    '/components/syx-popup',
                    '/components/syx-pagination',
                    '/components/syx-list'
                ]
            },
        ]
    },
    scss: {
        includePaths: [path.join(__dirname, '../../styles')]
    },
    markdown: {
        lineNumbers: true
    }
}
