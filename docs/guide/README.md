# Syx UI <img src="../../img/s.png" width="40" />

## 👀 介绍

本 UI 框架是基于 Vue 2 + ElementUI 实现的。

## 📌 项目特点

1. 二次封装 ElementUI ^2.13.2

## 🔮 视觉稿

ElementUI

## 🚲 为什么叫 Syx UI

Syx UI
